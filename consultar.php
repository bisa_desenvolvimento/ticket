<?php

require_once 'tickets.class.php';

$oTicket = new Tickets();
$status = $oTicket->getStatus();

if($_POST['acessar'] == 'getTickets'){
        getTickets($oTicket);  
}else{
 	    getTicketsTotal($oTicket);
}

function getTickets($oTicket)
{
	$data = empty($_POST["data"]) ? '': $_POST["data"];
	$status = empty($_POST["ticket"]) ? '': $_POST["ticket"];
	$suporte = empty($_POST["user"]) ? '': $_POST["user"];

	$dados = $oTicket->getTickets($suporte, $status, $data);

	echo json_encode($dados);
}

function getTicketsTotal($oTicket)
{
	$dadosTotal = $oTicket->getTicketsTotal();
	echo json_encode($dadosTotal);

}


?>