    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="assets/js/materialize.js"></script>
    
    <script src="assets/js/init.js"></script>
    <script>
      $(document).ready(function() {
        $('.modal-trigger').leanModal();
        // $('#modal2').leanModal();
      });

      function exibirDados(){
	    $('#listaTicket').html('');
         
         let data = $('[name=data]').val();
         let ticket = $('[name=tickets]:checked').val();
         let user = $('input[name="user"]:checked').toArray().map(function(check) { 
			    return $(check).val(); 
			});

        $.ajax({
        	url: 'consultar.php',
        	method: 'post',
        	data:{data, ticket, user, acessar: 'getTickets'},
        	dataType: 'json'
        }).done((resp) => {

        	let ticketsUser = [];
        	let html = '';
            let ticketMaster = ticket;

        	user.map((u) => {
        		ticketsUser.push(resp.filter(t => u == t.suporte_id));
        		
        	});

        	ticketsUser.map((ticket) => {
        		if (ticket.length > 0){
        			let concluidos =  `${ticket.filter(t => t.tickets_status === 'Concluído').length}`;
        			let naoConcluidos = `${ticket.filter(t => t.tickets_status === 'Não Concluído').length}`;

     //    			if (concluidos.length < 2) concluidos = '0' + concluidos;
					// if (naoConcluidos.length < 2) concluidos = '0' + concluidos;

                    if (ticketMaster === '2'){
                       html += `<p><strong style="font-weight:bold"> ${ticket[0].suporte.toUpperCase()} - ${naoConcluidos} ATENDIMENTO NÃO CONCLUÍDOS </strong></p><hr>`;
                    }else if (ticketMaster === '1'){
                        html += `<p><strong style="font-weight:bold"> ${ticket[0].suporte.toUpperCase()} - ${concluidos} ATENDIMENTO CONCLUÍDO </strong></p><hr>`;
                    }else{
                        html += `<p><strong style="font-weight:bold"> ${ticket[0].suporte.toUpperCase()} - ${concluidos} ATENDIMENTO CONCLUÍDO / ${naoConcluidos} ATENDIMENTO NÃO CONCLUÍDOS </strong></p><hr>`;
                    }

        			ticket.map((t) => {
			        	if (t.note) {
			               t.note = t.note.substring(0, 400) + "..." ;  
			        	}else {
						   t.note = '';
						}
        				html += `<li>
			        		<div class="card">
			         			<div class="card-content">

			         				<p>${t.suporte} - ${t.status} - ${t.tickets_status}</p>
			         				<p>Cliente: ${t.cliente}</p>
									<p>Resumo: ${t.resumo}</p>
			         				<p>Ticket: ${t.ticket}</p>

			         				<p> <strong style="font-weight:bold">Obs: </strong><i>${t.note}</i></p>
		        			</div>
			        		</div>
	        			</li>`;

        			})

        		}
        	})

        	let html2 = '';
        	let naoConcluidos = resp.filter(t => t.tickets_status === 'Não Concluído').length;
        	let concluidos = resp.filter(t => t.tickets_status === 'Concluído').length;

            if (ticket === '2'){
                html2 += `<p><strong style="font-weight:bold">TOTAL DE ATENDIMENTOS NÃO CONCLUÍDOS: ${naoConcluidos}</strong></p>
                          <br>`;
            }else if (ticket === '1'){
                html2 += `<P><strong style="font-weight:bold">TOTAL DE ATENDIMENTOS CONCLUÍDOS: ${concluidos}</strong></P>
                          <br>`;
            }else{
                html2 += `<p><strong style="font-weight:bold">TOTAL DE ATENDIMENTOS NÃO CONCLUÍDOS: ${naoConcluidos}</strong></p>
                      <P><strong style="font-weight:bold">TOTAL DE ATENDIMENTOS CONCLUÍDOS: ${concluidos}</strong></P>
                      <br>`;
            }


	        $('#listaTicket').html(html);
	        $('#listaTicketTotal').html(html2);
				
        });

        $.ajax({
        	url: 'consultar.php',
        	data:{acessar: 'getTicketsTotal'},
        	method: 'post',
        	dataType: 'json'
        }).done((resp) => {

        	let listaTicketGeral = '';
        	let statusPS = resp.filter(t => t.status === 'PENDÊNCIA SUPORTE').length;
        	let statusPHP = resp.filter(t => t.status === 'PENDÊNCIA PHP').length;
        	let statusPD = resp.filter(t => t.status === 'PENDÊNCIA DELPHI').length;
        	let statusD = resp.filter(t => t.status === 'DESFILIAÇÃOI').length;
        	let statusF = resp.filter(t => t.status === 'FILIAÇÃO').length;
        	let statusT = resp.filter(t => t.status === 'TRANSFERÊNCIA').length;
        	let statusES = resp.filter(t => t.status === 'ENVIO SIAPE').length;
        	let statusAR = resp.filter(t => t.status === 'AGUARDANDO RELATÓRIO').length;
        	let statusPC = resp.filter(t => t.status === 'PENDÊNCIA CLIENTE').length;
        	let statusB = resp.filter(t => t.status === 'BACKLOG').length;
        	let statusTR = resp.filter(t => t.status === 'TREINAMENTO').length;

		    listaTicketGeral += `<p><strong style="font-weight:bold">TICKETS COM PENDÊNCIA SUPORTE: ${statusPS}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM PENDÊNCIA PHP: ${statusPHP}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM PENDÊNCIA DELPHI: ${statusPD}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM DESFILIAÇÃO: ${statusD}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM FILIAÇÃO: ${statusF}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM TRANSFERÊNCIA: ${statusT}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM ENVIO SIAPE: ${statusES}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM AGUARDANDO RELATÓRIO: ${statusAR}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM PENDÊNCIA CLIENTE: ${statusPC}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM BACKLOG: ${statusB}</strong></p>
					<p><strong style="font-weight:bold">TICKETS COM TREINAMENTO: ${statusTR}</strong></p>`;

		$('#listaTicketGeral').html(listaTicketGeral);

        });

        $('#modal1').closeModal();
        $('#modal2').openModal();
      }

      function gerarDados(){
        $('#modal2').closeModal();
      }

      function voltar(){
        $('#modal2').closeModal();
        $('#modal1').openModal();
      }
    </script>
  </body>
</html>