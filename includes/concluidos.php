<div class="row">

<div class="col s12 m12">
  <div class="card cor-concluida darken-1 section scrollspy">
    <div class="card-content ">
        <?php if (count($tickets)){ ?>
        <table class="bordered">
            <thead>
                <tr>
                <th data-field="" class="coluna-mantis">Ticket</th>
                <th data-field="" class="coluna-resumo">Descrição</th>
                <th class="coluna-projeto hide-on-small-only" data-field="">Cliente</th>
                <th class="coluna-atribuido" data-field="">Responsável</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($tickets as $ticket){ ?>
                <tr>
                    <td><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=<?php echo $ticket->ticket?>'><?php echo $ticket->ticket?></a></td>
                    <td><?php echo $ticket->resumo?></td>
                    <td class='hide-on-small-only'><?php echo $ticket->cliente?></td>
                    <td><?php echo $ticket->suporte?></td>
                <tr>
            <?php } ?>
            </tbody>
        </table>
        <?php } else { ?>
            <h6>Nenhum ticket cadastrado.</h6>
        <?php } ?>
  </div>
</div>
</div>

</div>