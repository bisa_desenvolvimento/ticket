<div class="row">
<div class="col s12 m12" style="z-index: 50;">
<ul class="hide-on-med-and-down">
        <?php foreach ($status as $key => $value): 
            $total = (isset($tickets[$value])) ? count($tickets[$value]) : 0;  
        ?>
        <a class="menu_ cor-<?php echo $key?> btn col s3" href="#<?php echo $key?>"><?php echo $value ?>
        <?php if($total): ?> <span class="badge_ red lighten-2"><?php echo $total?></span> <?php endif; ?>
        </a>
        <?php endforeach;?>
      </ul>
    </div>
</div>

<div class="row">

<?php foreach ($status as $key => $value): ?>
<div class="col s12 m12">
  <div id="<?php echo $key?>" class="card cor-<?php echo $key?> darken-1 section scrollspy">
    <div class="card-content ">
        <span class="card-title"><?php echo $value?></span>
        <?php if (isset($tickets[$value])){ ?>
        <table class="bordered">
            <thead>
                <tr>
                <th data-field="" class="coluna-mantis">Ticket</th>
                <th data-field="" class="coluna-resumo">Descrição</th>
                <th class="coluna-projeto hide-on-small-only" data-field="">Cliente</th>
                <th class="coluna-atribuido" data-field="">Responsável</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($tickets[$value] as $ticket){ ?>
                <tr>
                    <td><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=<?php echo $ticket->ticket?>'><?php echo $ticket->ticket;?></a></td> 
                    <td>
                        <?php echo $ticket->resumo?><br>
                        <?php if($key == '02' or $key == '10'){ 
                            $mantis = $oTicket->getRelations($ticket->ticket);
                        ?>
                        <table>
                            <?php foreach( $mantis as $man){ ?>
                            <tr>
                                <td width="50%"><b>Mantis:</b> <a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=<?php echo $man->mantis?>'><?php echo $man->mantis?></a></td>
                                <td><b>Status:</b> <?php echo $man->status?></td>
                            </tr>
                            <?php } ?>
                        </table>
                        <?php } ?>
                    </td>
                    <td class='hide-on-small-only'><?php echo $ticket->cliente?></td>
                    <td><?php echo $ticket->suporte?></td>
                <tr>
            <?php } ?>
            </tbody>
        </table>
        <?php }else{ ?>
            <h6>Nenhum ticket cadastrado.</h6>
        <?php } ?>
  </div>
</div>
</div>
<?php endforeach;?>

</div>