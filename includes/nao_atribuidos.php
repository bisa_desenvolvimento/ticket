<div class="row">

<div class="col s12 m12">
  <div class="card cor-<?=$_GET['status'] == 10 ? 'nova' : 'concluida'?> darken-1 section scrollspy">
    <div class="card-content ">
        <span class="card-title"><?=@$value?></span>
        <?if (count($tickets)): ?>
        <table class="bordered">
            <thead>
                <tr>
                <th data-field="" class="coluna-mantis">Ticket</th>
                <th data-field="" class="coluna-resumo">Descrição</th>
                <th class="coluna-projeto hide-on-small-only" data-field="">Cliente</th>
                <th class="coluna-atribuido" data-field="">Responsável</th>
                </tr>
            </thead>
            <tbody>
            <?foreach ($tickets as $ticket): ?>
                <tr>
                    <td><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=<?=$ticket->ticket?>'><?=$ticket->ticket?></a></td>
                    <td><?=$ticket->resumo?></td>
                    <td class='hide-on-small-only'><?=$ticket->cliente?></td>
                    <td><?=$ticket->suporte?></td>
                <tr>
            <?endforeach;?>
            </tbody>
        </table>
        <?else: ?>
            <h6>Nenhum ticket cadastrado.</h6>
        <?endif;?>
  </div>
</div>
</div>

</div>