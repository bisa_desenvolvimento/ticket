<?php require_once 'includes/header.php'; ?>

<div class="row">
  <a style="float: right; margin-right: 50px;" class="waves-effect waves-light btn red modal-trigger" href="#modal1">Feedback diário</a>
	<div style="margin-top: 20%; margin-bottom: 15%;" align="center" class="col s12 m6 l8 offset-m6 offset-l4 container hide-on-med-and-up">
			<img data-caption="R. João Suassuna, 51 - Boa Vista, Recife - PE, 50050-350" height="150" width="250" src="http://sprint.bisaweb.com.br/materialize/img/header.jpg">
		</div>
		<div style="margin-top: 5%; margin-bottom: 5%;"; align="center" class="container hide-on-small-only">
			<img data-caption="R. João Suassuna, 51 - Boa Vista, Recife - PE, 50050-350" height="150" width="250" src="http://sprint.bisaweb.com.br/materialize/img/header.jpg">
		</div>

		<form action="lista.php">
			<div class="col s12 m6 l8 offset-m3 offset-l2 container">
				<select name="status" class="badge center">
				  <option value='A'>Não concluídos</option>
				  <option value='C'>Concluídos</option>
        </select>
				<span><input class="btn red col m6 offset-m3 s8 offset-s2 l6 offset-l3" type="submit" value="Exibir"></span>
			</div>
		</form>
</div>

<?php 
require_once 'tickets.class.php';
$oTicket = new Tickets();

$usuarios = $oTicket->getSuportes();
 // echo'<pre>'; var_dump($usuarios[0]->nome ); exit();
 
?>

<!-- Modal Structure -->
<div id="modal1" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Relatório de Feedback Diário</h4>
		<form action="#" method="post">
			<div>
					<p style="font-weight: bold">Funcinários</p>
					<ul>
					  <?php for($i = 0; $i < count($usuarios); ++$i) { ?>
						<li>
							<p>
								<input type="checkbox" id="<?php echo $usuarios[$i]->id ?>" name="user" value="<?php echo $usuarios[$i]->id ?>"/>
								<label for="<?php echo $usuarios[$i]->id ?>"><?php echo $usuarios[$i]->nome ?></label>
							</p>
						</li>

                      <?php } ?>
					</ul>
			</div>
			<div class="group">
				<p style="font-weight: bold;">Ticket / Data</p>
				<div style="display: flex;" id="DivA">
					<p style="margin-right: 20px;">
						<input name="tickets" type="radio" id="todos" value="0" checked/>
						<label for="todos">Todos</label>
					</p>
					<p style="margin-right: 20px;">
						<input name="tickets" type="radio" id="conc" value="1"  />
						<label for="conc">Concluído</label>
					</p>
					<p>
						<input name="tickets" type="radio" id="nconc" value="2" />
						<label for="nconc">Não Concluído</label>
					</p>
				</div>
				<div id="DivLateral">
					<input type="date" name="data" value='<?php echo date("Y-m-d"); ?>'>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<a href="javascript:formName.submit()" onclick="exibirDados()" class="waves-effect waves-red btn-flat">Exibir</a>
		</div>
	</form>
</div>

<div id="modal2" class="modal modal-fixed-footer">
    <div class="modal-content">
		<h4>Relatório de Feedback Diário</h4>
		<ul id="listaTicket">
			
<!-- 				<li>
				<div class="card">
								<div class="card-content">
									<p>Mayane Castro - PENDÊNCIA SUPORTE - (Concluído)</p>
									<p>Cliente: SINPROESEMMA-SIND TRABS EDUC REDES PUBL MUN E EST</p>
									<p>Ticket: 6993 - FILIADOSWEB</p>
									<hr>
									<p>
										<strong style="font-weight:bold">Obs:</strong>
										<i>Entrarei em contato a cliente ainda essa semana.</i>
									</p>
								</div>
							</div>
				</li> -->
		</ul>
		<ul>
			  <li>
				<div class="card">
					<div class="card-content">
					   <div id="listaTicketTotal">
<!-- 							<p><strong style="font-weight:bold">TOTAL DE ATENDIMENTOS NÃO CONCLUÍDOS: 10</strong></p>
							<P><strong style="font-weight:bold">TOTAL DE ATENDIMENTOS CONCLUÍDOS: 10</strong></P>
							<br> -->
					   </div>
					   <div id="listaTicketGeral">
<!-- 							<p><strong style="font-weight:bold">TICKETS COM PENDÊNCIA SUPORTE: 09</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM PENDÊNCIA PHP: 37</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM PENDÊNCIA DELPHI: 01</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM DESFILIAÇÃO: 00</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM FILIAÇÃO: 01</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM TRANSFERÊNCIA: 02</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM ENVIO SIAPE: 00</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM AGUARDANDO RELATÓRIO: 05</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM PENDÊNCIA CLIENTE: 03</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM LIGAR SINASEFE 00</strong></p>
							<p><strong style="font-weight:bold">TICKETS COM TREINAMENTO:00</strong></p> -->
					   </div>
					</div>
				 </div>
				</li>
		</ul>
    </div>
    <div class="modal-footer">
		  <a href="#!" onclick="voltar()" class="waves-effect waves-red btn-flat">Voltar</a>	
			<a href="#!" onclick="gerarDados()" class="waves-effect waves-red btn-flat"></a>
    </div>
  </div>


<?php require_once 'includes/footer.php'; ?>
