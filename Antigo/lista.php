<?
require_once 'tickets.class.php';
$oTicket = new Tickets();

$status = $oTicket->getStatus();

$tipo = $_GET['status'] == 'A' ? 'NÃO CONCLUÍDOS' : 'CONCLUÍDOS';

require_once 'includes/header.php';

?>

<div class="row">
  <div id="top" class="col s12 m4 offset-m4 section scrollspy">
    <div class="center white-text card-panel red">
      <span class=""><h5>TICKETS <?=$tipo?></h5></span>
    </div>
  </div>
</div>

<? 
    if ($_GET['status'] == 'A') {
        $tickets = $oTicket->getTicketsNaoConcluidos(@$_GET['suporte']);
        require_once 'includes/nao_concluidos.php';
    } else {
        $tickets = $oTicket->getTicketsConcluidos(@$_GET['suporte']);
        require_once 'includes/concluidos.php';
    }
?>


<div class="fixed-action-btn" style="bottom: 15px; right: 15px;">
    <a href="#top" class="btn-floating btn-large red">
        <i class="large material-icons">navigation</i>
    </a>
</div>

<!--div class="fixed-action-btn" style="bottom: 80px; right: 15px;">
    <a href="/" class="btn-floating btn-large indigo">
        <i class="large material-icons">navigation</i>
    </a>
</div-->


<? require_once 'includes/footer.php'; ?>
