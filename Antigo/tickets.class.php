<?php

class Tickets
{

    private $pdo;

    public function __construct()
    {
        define('HOST', 'mysql.bisaweb.com.br');
        define('USER', 'bisaweb');
        define('PASS', 'bisa2000');
        define('BASE', 'bisaweb_bugtracker');

        try {
            $this->pdo = new PDO( 'mysql:host=' . HOST . ';dbname=' . BASE, USER, PASS );
        }
        catch ( PDOException $e ) {
            echo 'Erro ao conectar com o MySQL: ' . $e->getMessage();
        }

        $this->pdo->exec("set names utf8");
    }

    public function getTicketsNaoConcluidos($suporte = null)
    {
        $sql = "SELECT 
                    b.id AS ticket, b.summary AS resumo, u.realname AS suporte,
                    (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 25 AND cf.bug_id = b.id) AS status,
                    (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = b.id) AS cliente
                FROM
                    mantis_bug_table b 
                    LEFT JOIN mantis_user_table u ON b.handler_id = u.id
                WHERE b.project_id = 39
                AND b.status IN (10, 50)";

        if($suporte) {
            $sql .= " AND handler_id = $suporte";
        }

        $sql .= " ORDER BY b.last_updated DESC";

        $result = $this->pdo->query($sql);
        $rows = $result->fetchAll(PDO::FETCH_OBJ);

        foreach ( $rows as $row ) {
            $tickets[$row->status][] = $row;
        }

        return $tickets;
    }

    public function getRelations($ticket)
    {
        $sql = "SELECT b.id AS mantis,
                    CASE b.status 
                    WHEN 10 THEN 'novo'
                    WHEN 20 THEN 'retorno'
                    WHEN 30 THEN 'admitido'
                    WHEN 40 THEN 'aprovado'
                    WHEN 50 THEN 'atribuido'
                    WHEN 60 THEN 'impedimento'
                    WHEN 70 THEN 'teste'
                    WHEN 80 THEN 'resolvido'
                    WHEN 90 THEN 'fechado'
                    END AS status
                FROM
                    mantis_bug_relationship_table br 
                    INNER JOIN mantis_bug_table b 
                    ON br.destination_bug_id = b.id 
                WHERE br.source_bug_id = $ticket";

        $result = $this->pdo->query($sql);
        return $result->fetchAll(PDO::FETCH_OBJ);
    }

    public function getTicketsConcluidos($suporte = null)
    {
        $sql = "SELECT 
                    b.id AS ticket, b.summary AS resumo, u.realname AS suporte,
                    (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 25 AND cf.bug_id = b.id) AS status,
                    (SELECT cf.value FROM mantis_custom_field_string_table cf WHERE cf.field_id = 1 AND cf.bug_id = b.id) AS cliente
                FROM
                    mantis_bug_table b 
                    LEFT JOIN mantis_user_table u ON b.handler_id = u.id
                WHERE b.project_id = 39
                AND b.status = 90";

        if($suporte) {
            $sql .= " AND handler_id = $suporte";
        }

        $sql .= " ORDER BY b.last_updated DESC";

        $result = $this->pdo->query($sql);
        return $result->fetchAll(PDO::FETCH_OBJ);
    }

    public function getStatus()
    {
        return [
            '01' => 'PENDÊNCIA SUPORTE',
            '02' => 'PENDÊNCIA PHP',
            '03' => 'PENDÊNCIA DELPHI',
            '04' => 'ENVIO SIAPE',
            '05' => 'DESFILIAÇÃO',
            '06' => 'FILIAÇÃO',
            '07' => 'ALTERAÇÃO',
            '08' => 'AGUARDANDO RELATÓRIO',
            //'09' => 'AGUARDANDO D8',
            '10' => 'BACKLOG',
            '11' => 'PENDÊNCIA CLIENTE',
            '12' => 'TREINAMENTO',
            '13' => 'TRANSFERÊNCIA',
            '14' => 'ATRIBUIDOS',
        ];
    }

    public function getSuportes()
    {
        $sql = "SELECT 
                    u.id, u.realname AS nome
                FROM
                    mantis_project_user_list_table pu 
                    INNER JOIN mantis_user_table u 
                    ON pu.user_id = u.id 
                WHERE pu.project_id = 39 
                    AND u.enabled = 1
                ORDER BY u.realname";

        $result = $this->pdo->query($sql);
        return $result->fetchAll(PDO::FETCH_OBJ);
    }

}
