<!DOCTYPE html>
<html lang="en">
<head>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href=""  media="screen,projection"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="theme-color" content="#000000">
  <link href="assets/css/materialize.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <title>::- Tickets - Bisaweb -::</title>
  </head>
  <body class="">

    <nav>
      <div class="black nav-wrapper">
        <a class="brand-logo" href="/"><h3><img class="circle white z-depth-3" width="100" height="50"src="http://sprint.bisaweb.com.br/materialize/img/Logo.png" alt="" /></h3></a>
        <a href="#" data-activates="mobile-demo" class="button-collapse " id="nav-btn"><i class="material-icons">menu</i></a>

        <?
        if (isset($_GET['status'])):
            $suportes = $oTicket->getSuportes();
        ?>
	        <div class="col s12 m6 l8 offset-m3 offset-l2 container">
	            <select id="suportes" class="badge center">
                <option value=''>Todos</option>
                <?foreach ($suportes as $suporte): ?>
                <option value='<?=$suporte->id?>'><?=$suporte->nome?></option>
                <?endforeach;?>
            </select>
		      </div>
        <?endif;?>

      </div>
    </nav>