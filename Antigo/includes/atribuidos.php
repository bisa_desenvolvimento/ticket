<div class="row">
<div class="col s12 m12" style="z-index: 50;">
      <ul class="section table-of-contents">
        <?foreach ($status as $key => $value): ?>
        <a class="cor-<?=$key?> btn col s3" href="#<?=$key?>"><b><?=$value?></b></a>
        <?endforeach;?>
      </ul>
    </div>
</div>

<div class="row">

<?foreach ($status as $key => $value): ?>
<div class="col s12 m12">
  <div id="<?=$key?>" class="card cor-<?=$key?> darken-1 section scrollspy">
    <div class="card-content ">
        <span class="card-title"><?=$value?></span>
        <?if (isset($tickets[$value])): ?>
        <table class="bordered">
            <thead>
                <tr>
                <th data-field="" class="coluna-mantis">Ticket</th>
                <th data-field="" class="coluna-resumo">Descrição</th>
                <th class="coluna-projeto hide-on-small-only" data-field="">Cliente</th>
                <th class="coluna-atribuido" data-field="">Responsável</th>
                </tr>
            </thead>
            <tbody>
            <?foreach ($tickets[$value] as $ticket): ?>
                <tr>
                    <td><a target='_blank' href='http://mantis.bisaweb.com.br/view.php?id=<?=$ticket->ticket?>'><?=$ticket->ticket?></a></td>
                    <td><?=$ticket->resumo?></td>
                    <td class='hide-on-small-only'><?=$ticket->cliente?></td>
                    <td><?=$ticket->suporte?></td>
                <tr>
            <?endforeach;?>
            </tbody>
        </table>
        <?else: ?>
            <h6>Nenhum ticket cadastrado.</h6>
        <?endif;?>
  </div>
</div>
</div>
<?endforeach;?>

</div>