$(document).ready(function() {

    $.urlParam = function(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        } else {
            return decodeURI(results[1]) || 0;
        }
    }

    $(".button-collapse").sideNav();

    $('.scrollspy').scrollSpy();

    $('select').material_select();

    $('#suportes').change(function() {
        var suporte = $(this).val();
        var url = window.location.pathname;
        window.location.replace(url + '?status=' + $.urlParam('status') + '&suporte=' + suporte);
    });

    if ($.urlParam('suporte')) {
        $('#suportes').val($.urlParam('suporte'));
        $('select').material_select();
    }

});